﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class ControladorCamara : MonoBehaviour {
    public GameObject player;

    private Vector3 offset;

    public AudioClip loop;

    IEnumerator Start()
    {
        offset = transform.position - player.transform.position;
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
        yield return new WaitForSeconds(audio.clip.length);
        audio.clip = loop;
        audio.loop = true;
        audio.Play();
    }

   
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }

}
