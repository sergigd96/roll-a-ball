﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorEsfera : MonoBehaviour {


    public float speed;
    private int count;
    public Text score;
    public Text win;
    private AudioSource audio;
    public AudioClip[] audioSource;
    private AudioClip clip;
    

    void Start()
    {
        count = 0;
        UpdateCounter();
        audio = gameObject.GetComponent<AudioSource>();
        win.text = "";
    }

    void FixedUpdate () {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horizontal, 0, vertical);

        GetComponent<Rigidbody>().AddForce(direction * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "pickup")
        {
            Destroy(other.gameObject);
            count++;
            RandomSound();
            UpdateCounter();
            

        }
    }

   

    private void UpdateCounter()
    {
        score.text = "Puntos :" + count;

        int numPickups = GameObject.FindGameObjectsWithTag("pickup").Length;

        if (numPickups == 1)
        {
            win.text = "Has Ganado!";
        }

    }

    private void RandomSound()
    {
        int index = UnityEngine.Random.Range(0, audioSource.Length);
        clip = audioSource[index];
        audio.clip = clip;
        audio.Play();
    }

}
